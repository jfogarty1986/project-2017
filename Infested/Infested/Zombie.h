#ifndef ZOMBIE_H
#define ZOMBIE_H
#include "Enemy.h"
#include "BulletHandler.h"

int move = 0;

class Zombie : public Enemy
{
public:

	virtual ~Zombie() {}

	Zombie() : Enemy()
	{
		m_dyingTime = 50;
		m_health = 10;
		m_moveSpeed = 3;
		m_bulletFiringSpeed = 0;
	}

	virtual void collision()
	{
		m_health -= 1;

		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
			}

		}
	}

	virtual void update()
	{
		if (!m_bDying)
		{

			if (move == 0)
			{
				m_angle = 180;
				m_velocity.setX(-m_moveSpeed);

			}
			else if (move == 1)
			{
				m_angle = 0;
				m_velocity.setX(m_moveSpeed);
			}
			

			if (m_position.getX() >= TheGame::Instance()->getGameWidth() - 150)
			{
				move = 0;
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, -3));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, 3));
			}
			else if (m_position.getX() <= 50)
			{
				move = 1;
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, -3));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, 3));
			}
			else if (m_position.getX() >= TheGame::Instance()->getGameWidth() - 50)
			{
				move = 4;
			}
			else if (m_position.getX() <= 50)
			{
				move = 3;
			}




		}
		else
		{
			m_velocity.setY(0);
			doDyingAnimation();
		}



		ShooterObject::update();
	}

private:
	int move = 0;	
};

class ZombieCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Zombie();
	}
};


#endif // !ZOMBIE_H
