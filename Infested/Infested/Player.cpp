

#include "Player.h"
#include "Game.h"
#include "InputHandler.h"
#include "TileLayer.h"
#include "BulletHandler.h"
#include "SoundManager.h"
#include "CollisionManager.h"
#include <stdlib.h>


using namespace std;

Player* Player::s_pInstance = 0;


Player::Player() : ShooterObject(),
m_invulnerable(false),
m_invulnerableTime(300),
m_invulnerableCounter(0)
{
	m_moveSpeed = 10;
}

void Player::collision()
{
    // if the player is not invulnerable then set to dying and change values for death animation tile sheet
    if(!m_invulnerable && !TheGame::Instance()->getLevelComplete())
    {
        m_textureID = "largeexplosion";
        m_currentFrame = 0;
        m_numFrames = 9;
        m_width = 60;
        m_height = 60;
        m_bDying = true;
    }
}
void Player::tileCollision()
{
	// if the player is not invulnerable then set to dying and change values for death animation tile sheet
	if (!m_invulnerable && !TheGame::Instance()->getLevelComplete())
	{
		
		/**if (TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_D) && ) {
			m_bDying = false;
			m_moveSpeed = 0;
		}
		else
		{
			m_moveSpeed = 3;
		}**/
	}
}

//void Player::setMoveSpeed(int speed) {
//	m_moveSpeed = speed;
//	//return m_moveSpeed;
//}

void Player::load(std::unique_ptr<LoaderParams> const &pParams)
{
    // inherited load function
    ShooterObject::load(std::move(pParams));
    
    // can set up the players inherited values here
    
    // set up bullets
    m_bulletFiringSpeed = 13;
    m_moveSpeed = 3;
    
    // we want to be able to fire instantly
    m_bulletCounter = m_bulletFiringSpeed;
    
    // time it takes for death explosion
    m_dyingTime = 100;
}

void Player::draw()
{
    // player has no special drawing requirements
    ShooterObject::draw();
}

void Player::handleAnimation()
{
    // if the player is invulnerable we can flash its alpha to let people know
    if(m_invulnerable)
    {
        // invulnerability is finished, set values back
        if(m_invulnerableCounter == m_invulnerableTime)
        {
            m_invulnerable = false;
            m_invulnerableCounter = 0;
            m_alpha = 255;
        }
        else // otherwise, flash the alpha on and off
        {
            if(m_alpha == 255)
            {
                m_alpha = 0;
            }
            else
            {
                m_alpha = 255;
            }
        }
        
        // increment our counter
        m_invulnerableCounter++;
    }
    
    // if the player is not dead then we can change the angle with the velocity to give the impression of a moving helicopter
    /*if(!m_bDead)
    {
        if(m_velocity.getX() < 0)
        {
            m_angle = -90.0;
        }
        else if(m_velocity.getX() > 0)
        {
            m_angle = 90.0;
        }
        else
        {
            m_angle = 0.0;
        }
    }*/
    
    // our standard animation code - for helicopter propellors
    m_currentFrame = int(((SDL_GetTicks() / (100)) % m_numFrames));
}

void Player::update()
{
	
    if(TheGame::Instance()->getLevelComplete())
    {
        if(m_position.getX() >= TheGame::Instance()->getGameWidth())
        {
            TheGame::Instance()->setCurrentLevel(TheGame::Instance()->getCurrentLevel() + 1);
        }
        else
        {
            m_velocity.setY(0);
            m_velocity.setX(3);
            ShooterObject::update();
            handleAnimation();
        }
    }
    else
    {
        // if the player is not doing its death animation then update it normally
        if(!m_bDying)
        {
            // reset velocity
            m_velocity.setX(0);
            m_velocity.setY(0);
            
            // get input
            handleInput();
            
			if (TheGame::Instance()->getCurrentLevel() == 2)
			{
				m_moveSpeed = 8;
			}
            // do normal position += velocity update
            ShooterObject::update();
            
            // update the animation
            handleAnimation();
        }
        else // if the player is doing the death animation
        {
            m_currentFrame = int(((SDL_GetTicks() / (100)) % m_numFrames));
            
            // if the death animation has completed
            if(m_dyingCounter == m_dyingTime)
            {
                // ressurect the player
                ressurect();
            }
            m_dyingCounter++;
        }
    }
	/*if (m_position.getY() + m_moveSpeed > TheGame::Instance()->getGameHeight())
	{
		m_position.setY(m_position.getY() - m_moveSpeed);
	}*/
	if (TheGame::Instance()->getCurrentLevel() == 2)
	{
		if (m_position.getX() > TheGame::Instance()->getGameWidth() - 150)
		{
			TheGame::Instance()->setLevelComplete(true);
		}
	}
}

void Player::ressurect()
{
    TheGame::Instance()->setPlayerLives(TheGame::Instance()->getPlayerLives() - 1);
    
    m_position.setX(543);
    m_position.setY(351);
    m_bDying = false;
    
    m_textureID = "player";
    
	if (TheGame::Instance()->getCurrentLevel() != 2)
	{
		m_currentFrame = 1;
		m_numFrames = 1;
		m_width = 101;
		m_height = 46;
		m_angle = 0;
	}
	else
	{
		m_currentFrame = 1;
		m_numFrames = 1;
		m_width = 140;
		m_height = 60;
		m_angle = 0;
	}
    
    
    m_dyingCounter = 0;
    m_invulnerable = true;
}

void Player::clean()
{
    ShooterObject::clean();
}

void Player::handleInput()
{
    if(!m_bDead)
    {
		const Uint8 *state = SDL_GetKeyboardState(NULL);
        // handle keys
		if (state[SDL_SCANCODE_W] && state[SDL_SCANCODE_A] && m_position.getX() > 32 && m_position.getY() > 32)
		{
			m_angle = -135;
			m_velocity.setY(-m_moveSpeed);
			m_velocity.setX(-m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			}
			
		}
		else if (state[SDL_SCANCODE_W] && state[SDL_SCANCODE_D] && m_position.getX() < TheGame::Instance()->getGameWidth() - 96 && m_position.getY() > 32)
		{
			m_angle = -45;
			m_velocity.setY(-m_moveSpeed);
			m_velocity.setX(m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			}
		}
		else if (state[SDL_SCANCODE_S] && state[SDL_SCANCODE_A] && m_position.getX() > 32 && m_position.getY() < 710)
		{
			m_angle = -225;
			m_velocity.setY(m_moveSpeed);
			m_velocity.setX(-m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			}
		}
		else if (state[SDL_SCANCODE_S] && state[SDL_SCANCODE_D] && m_position.getX() < TheGame::Instance()->getGameWidth() - 96 && m_position.getY() < 710)
		{
			m_angle = 45;
			m_velocity.setY(m_moveSpeed);
			m_velocity.setX(m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			};
		}
		else if(state[SDL_SCANCODE_W] && m_position.getY() > 0 && upMove == true)
        {
			m_angle = -90;
            m_velocity.setY(-m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			}
        }
        else if(state[SDL_SCANCODE_S] && (m_position.getY() + m_height) < TheGame::Instance()->getGameHeight() - 10 && downMove == true)
        {
			m_angle = 90;
            m_velocity.setY(m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			}
        }
	  
        else if(state[SDL_SCANCODE_A] && m_position.getX() > 0 && leftMove == true)
        {
			m_angle = 180;
            m_velocity.setX(-m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			}
        }
        else if(state[SDL_SCANCODE_D] && (m_position.getX() + m_width) < TheGame::Instance()->getGameWidth() && rightMove == true)
        {
			m_angle = 0;
            m_velocity.setX(m_moveSpeed);
			if (TheGame::Instance()->getCurrentLevel() != 2)
			{
				checkShooting();
			}
        }
		
		else if (TheInputHandler::Instance()->getMouseButtonState(0))
		{
			if (m_bulletCounter == m_bulletFiringSpeed)
			{
				TheSoundManager::Instance()->playSound("shoot", 0);
				if (m_angle == -90)
				{
					TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 50, m_position.getY() - 12, bulletSize, bulletSize, "bullet1", 1, Vector2D(0, -10));
				}
				else if (m_angle == 90)
				{
					TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 40, m_position.getY() + 50, bulletSize, bulletSize, "bullet1", 1, Vector2D(0, 10));
				}
				else if (m_angle == 180)
				{
					TheBulletHandler::Instance()->addPlayerBullet(m_position.getX(), m_position.getY() + 12, bulletSize, bulletSize, "bullet1", 1, Vector2D(-10, 0));
				}
				else
				{
					TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 88, m_position.getY() + 23, bulletSize, bulletSize, "bullet1", 1, Vector2D(10, 0));
				}
				//TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 90, m_position.getY() + 12, 11, 11, "bullet1", 1, Vector2D(10,0));
				m_bulletCounter = 0;
				bulletpowerupcounter++;
			}

			m_bulletCounter++;
		}
		
        else
        {
            m_bulletCounter = m_bulletFiringSpeed;
        }  
		
        
    }
}

void Player::usePower(std::vector<GameObject*> &objects)
{
	TheSoundManager::Instance()->playSound("nuke", 0);
	gameObjects = objects;
	hasPowerUp = true;
	
	
	for (std::vector<GameObject*>::const_iterator it = objects.begin(); it != objects.end(); ++it)
	{
		if ((*it)->isVisible() == true && (*it)->type() != std::string("Boss"))
		{
			(*it)->dying();
			(*it)->collision();
		}
	}
	
	
	/*for (int i = 0; i < objects.size(); i++)
	{
		if(objects[i]->isVisible() == true && objects[i]->type() != std::string("Boss"))
		{
			objects[i]->dying();
			objects[i]->collision();
		}
	}*/
	
}

int Player::getX()
{
	return m_position.getX();
}

int Player::getY()
{
	return m_position.getY();
}

void Player::setPosition(int x, int y)
{
	m_position.setX(x);
	m_position.setY(y);
}

void Player::activatePower()
{
	m_moveSpeed = 8;
	m_bulletFiringSpeed = 4;
}

void Player::checkShooting()
{
	if (TheInputHandler::Instance()->getMouseButtonState(0))
	{
		if (m_bulletCounter == m_bulletFiringSpeed)
		{
			bulletpowerupcounter++;
			TheSoundManager::Instance()->playSound("shoot", 0);
			if (m_angle == -90)
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 50, m_position.getY() - 12, bulletSize, bulletSize, "bullet1", 1, Vector2D(0, -10));
			}
			else if (m_angle == 90)
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 40, m_position.getY() + 50, bulletSize, bulletSize, "bullet1", 1, Vector2D(0, 10));
			}
			else if (m_angle == 180)
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX(), m_position.getY() + 12, bulletSize, bulletSize, "bullet1", 1, Vector2D(-10, 0));
			}
			// A + S
			else if (m_angle == -225)
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX(), m_position.getY() + 50, bulletSize, bulletSize, "bullet1", 1, Vector2D(-10, 10));
			}
			// W + A
			else if (m_angle == -135)
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 15, m_position.getY() - 25, bulletSize, bulletSize, "bullet1", 1, Vector2D(-10, -10));
			}
			// S + D
			else if (m_angle == 45)
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 65, m_position.getY() + 50, bulletSize, bulletSize, "bullet1", 1, Vector2D(10, 10));
			}
			// W + D
			else if (m_angle == -45)
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 75, m_position.getY(), bulletSize, bulletSize, "bullet1", 1, Vector2D(10, -10));
			}
			else
			{
				TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 88, m_position.getY() + 23, bulletSize, bulletSize, "bullet1", 1, Vector2D(10, 0));
			}
			//TheBulletHandler::Instance()->addPlayerBullet(m_position.getX() + 90, m_position.getY() + 12, 11, 11, "bullet1", 1, Vector2D(10,0));
			m_bulletCounter = 0;
			
		}

		m_bulletCounter++;
		if (bulletpowerupcounter > 40)
		{
			m_moveSpeed = 3;
			m_bulletFiringSpeed = 13;
			bulletpowerupcounter = 0;
		}
	}
	
}

void Player::checkPowerUp(std::vector<GameObject*> &objects)
{
	int target = rand() % 3 + 1;
	switch (target)
	{
	case 1:
		speedPower();
		break;
	case 2:
		bulletSpeed();
		break;
	case 3:
		nuke(objects);
		break;
	case 4:
		life();
		break;
	default:
		0;
	}
}

void Player::speedPower()
{
	m_moveSpeed = 10;
}
void Player::bulletSpeed()
{
	m_bulletFiringSpeed = 4;
}
void Player::nuke(std::vector<GameObject*> &objects)
{
	TheSoundManager::Instance()->playSound("nuke", 0);
	gameObjects = objects;
	hasPowerUp = true;


	for (std::vector<GameObject*>::const_iterator it = objects.begin(); it != objects.end(); ++it)
	{
		if ((*it)->isVisible() == true && (*it)->type() != std::string("Boss"))
		{
			(*it)->dying();
			(*it)->collision();
		}
	}
}
void Player::life()
{
	TheGame::Instance()->addLife();
}

void Player::disableRight()
{
	rightMove = false;
}
void Player::disableLeft()
{
	leftMove = false;
}
void Player::disableUp()
{
	upMove = false;
}
void Player::disableDown()
{
	downMove = false;
}

void Player::enableUp()
{
	upMove = true;
}
void Player::enableDown()
{
	downMove = true;
}
void Player::enableLeft()
{
	leftMove = true;
}
void Player::enableRight()
{
	rightMove = true;
}

void Player::setAngle(int setAngle) {
	m_alpha = setAngle;
}


