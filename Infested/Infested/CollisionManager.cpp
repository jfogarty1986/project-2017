#include "CollisionManager.h"
#include "Collision.h"
#include "Player.h"
#include "Level.h"
#include "Enemy.h"
#include "BulletHandler.h"
#include "TileLayer.h"
#include "InputHandler.h"
#include "PowerUp.h"
#include "ShooterEnemy.h"
#include "ShooterObject.h"
#include "GameObject.h"

void CollisionManager::checkPlayerEnemyBulletCollision(Player* pPlayer)
{
    SDL_Rect* pRect1 = new SDL_Rect();
    pRect1->x = pPlayer->getPosition().getX();
    pRect1->y = pPlayer->getPosition().getY();
    pRect1->w = pPlayer->getWidth();
    pRect1->h = pPlayer->getHeight();
    
    for(int i = 0; i < TheBulletHandler::Instance()->getEnemyBullets().size(); i++)
    {
        EnemyBullet* pEnemyBullet = TheBulletHandler::Instance()->getEnemyBullets()[i];
        
        SDL_Rect* pRect2 = new SDL_Rect();
        pRect2->x = pEnemyBullet->getPosition().getX();
        pRect2->y = pEnemyBullet->getPosition().getY();
        
        pRect2->w = pEnemyBullet->getWidth();
        pRect2->h = pEnemyBullet->getHeight();
        
        if(RectRect(pRect1, pRect2))
        {
            if(!pPlayer->dying() && !pEnemyBullet->dying())
            {
                pEnemyBullet->collision();
                pPlayer->collision();
            }
        }
        
        delete pRect2;
    }
    
    delete pRect1;

	
}

void CollisionManager::checkEnemyPlayerBulletCollision(const std::vector<GameObject *> &objects)
{
    for(int i = 0; i < objects.size(); i++)
    {
        GameObject* pObject = objects[i];
        
        for(int j = 0; j < TheBulletHandler::Instance()->getPlayerBullets().size(); j++)
        {
            if(pObject->type() != std::string("Enemy") ||  !pObject->updating())
            {
                continue;
            }
            
            SDL_Rect* pRect1 = new SDL_Rect();
            pRect1->x = pObject->getPosition().getX();
            pRect1->y = pObject->getPosition().getY();
            pRect1->w = pObject->getWidth();
            pRect1->h = pObject->getHeight();
            
            PlayerBullet* pPlayerBullet = TheBulletHandler::Instance()->getPlayerBullets()[j];
            
            SDL_Rect* pRect2 = new SDL_Rect();
            pRect2->x = pPlayerBullet->getPosition().getX();
            pRect2->y = pPlayerBullet->getPosition().getY();
            pRect2->w = pPlayerBullet->getWidth();
            pRect2->h = pPlayerBullet->getHeight();

			
            
            if(RectRect(pRect1, pRect2))
            {
                if(!pObject->dying() && !pPlayerBullet->dying() && pObject->isVisible() == true)
                {
					
                    pPlayerBullet->collision();
					pObject->collision();
                    
                }
                
            }
            
            delete pRect1;
            delete pRect2;
        }
    }
}

void CollisionManager::checkPlayerEnemyCollision(Player* pPlayer, const std::vector<GameObject*> &objects)
{
    SDL_Rect* pRect1 = new SDL_Rect();
    pRect1->x = pPlayer->getPosition().getX();
    pRect1->y = pPlayer->getPosition().getY();
    pRect1->w = pPlayer->getWidth();
    pRect1->h = pPlayer->getHeight();
    
    for(int i = 0; i < objects.size(); i++)
    {
        if(objects[i]->type() != std::string("Enemy") || !objects[i]->updating())
        {
            continue;
        }
        
        SDL_Rect* pRect2 = new SDL_Rect();
        pRect2->x = objects[i]->getPosition().getX();
        pRect2->y = objects[i]->getPosition().getY();
        pRect2->w = objects[i]->getWidth();
        pRect2->h = objects[i]->getHeight();
        
        if(RectRect(pRect1, pRect2))
        {
            if(!objects[i]->dead() && !objects[i]->dying())
            {
				if (objects[i]->isVisible() == true)
				{
					pPlayer->collision();
				}
            }
        }
        
        delete pRect2;
    }
    
    delete pRect1;
}

void CollisionManager::checkPlayerTileCollision(Player* pPlayer, const std::vector<TileLayer*>& collisionLayers)
{
	playerX = pPlayer->getX();
	playerY = pPlayer->getY();
    for(std::vector<TileLayer*>::const_iterator it = collisionLayers.begin(); it != collisionLayers.end(); ++it)
    {
        TileLayer* pTileLayer = (*it);
        std::vector<std::vector<int>> tiles = pTileLayer->getTileIDs();
		
        Vector2D layerPos = pTileLayer->getPosition();
        
        int x, y, tileColumn, tileRow, tileid = 0;
		int colX, colY;
        
        x = layerPos.getX() / pTileLayer->getTileSize();
        y = layerPos.getY() / pTileLayer->getTileSize();
        
        if(pPlayer->getVelocity().getX() >= 0 || pPlayer->getVelocity().getY() >= 0)
        {
            tileColumn = ((pPlayer->getPosition().getX() + pPlayer->getWidth() - 48) / pTileLayer->getTileSize());
            tileRow = ((pPlayer->getPosition().getY() + pPlayer->getHeight() - 32) / pTileLayer->getTileSize());
			colX = tileColumn;
			colY = tileRow;
            tileid = tiles[tileRow + y][tileColumn + x];
			
        }
        else if(pPlayer->getVelocity().getX() < 0 || pPlayer->getVelocity().getY() < 0)
        {
            tileColumn = pPlayer->getPosition().getX() / pTileLayer->getTileSize();
            tileRow = pPlayer->getPosition().getY() / pTileLayer->getTileSize();
			colX = tileColumn;
			colY = tileRow;
            tileid = tiles[tileRow + y][tileColumn + x];
			
        }
        
        if(tileid != 0)
        {
			//pPlayer->collision();
			//pPlayer->setX(playerX);
			//pPlayer->setY(playerY);
			if (pPlayer->getAngle() == 0)
			{
				pPlayer->disableRight();
			}
			else if (pPlayer->getAngle() == -90)
			{
				pPlayer->disableUp();
			}
			else if (pPlayer->getAngle() == 180)
			{
				pPlayer->disableLeft();
			}
			else if (pPlayer->getAngle() == 90)
			{
				pPlayer->disableDown();
			}
			else if (pPlayer->getAngle() == -135)
			{
				pPlayer->setX(pPlayer->getX()+1);
				pPlayer->setY(pPlayer->getY()+1);
			}
			else if (pPlayer->getAngle() == -45)
			{
				pPlayer->setX(pPlayer->getX() - 1);
				pPlayer->setY(pPlayer->getY() + 1);
				
			}
			else if (pPlayer->getAngle() == -225)
			{
				pPlayer->setX(pPlayer->getX() + 1);
				pPlayer->setY(pPlayer->getY() - 1);
				
			}
			else if (pPlayer->getAngle() == 45)
			{
				pPlayer->setX(pPlayer->getX() - 1);
				pPlayer->setY(pPlayer->getY() - 1);
				
			}
        }
		else
		{
			pPlayer->enableDown();
			pPlayer->enableLeft();
			pPlayer->enableRight();
			pPlayer->enableUp();
		}
    }
}
//
void CollisionManager::checkEnemyBulletTileCollision(const std::vector<GameObject*> &objects, const std::vector<TileLayer*>& collisionLayers)
{
	for (std::vector<TileLayer*>::const_iterator it = collisionLayers.begin(); it != collisionLayers.end(); ++it)
	{
		TileLayer* pTileLayer = (*it);
		std::vector<std::vector<int>> tiles = pTileLayer->getTileIDs();
		Vector2D layerPos = pTileLayer->getPosition();

		int x, y, tileColumn, tileRow, tileid = 0;
		int colX, colY;
		
		for (int i = 0; i < TheBulletHandler::Instance()->getEnemyBullets().size(); i++)
		{

			EnemyBullet* pEnemyBullet = TheBulletHandler::Instance()->getEnemyBullets()[i];
			if (pEnemyBullet->getPosition().getX() < 32 || pEnemyBullet->getPosition().getX() > TheGame::Instance()->getGameWidth() - 32)
			{
				pEnemyBullet->collision();
			}
			else if (pEnemyBullet->getPosition().getY() < 32 || pEnemyBullet->getPosition().getY() > TheGame::Instance()->getGameHeight() - 32)
			{
				pEnemyBullet->collision();
			}
			SDL_Rect* Rect1 = new SDL_Rect();
			x = layerPos.getX() / pTileLayer->getTileSize();
			y = layerPos.getY() / pTileLayer->getTileSize();

			if (pEnemyBullet->getVelocity().getX() >= 0 || pEnemyBullet->getVelocity().getY() >= 0)
			{
				tileColumn = ((pEnemyBullet->getPosition().getX() + pEnemyBullet->getWidth() - 48) / pTileLayer->getTileSize());
				tileRow = ((pEnemyBullet->getPosition().getY() + pEnemyBullet->getHeight() - 32) / pTileLayer->getTileSize());
				colX = tileColumn;
				colY = tileRow;
				tileid = tiles[tileRow + y][tileColumn + x];

			}
			if (tileid != 0)
			{
				pEnemyBullet->collision();
			}
			
		}
	}
}


 void CollisionManager::checkPowerUpCollision(Player* pPlayer, std::vector<GameObject*> &objects)
{
	PowerUp *pickedUp;
	SDL_Rect* pRect1 = new SDL_Rect();
	SDL_Rect* pRect2 = new SDL_Rect();
	pRect1->x = pPlayer->getPosition().getX();
	pRect1->y = pPlayer->getPosition().getY();
	pRect1->w = pPlayer->getWidth();
	pRect1->h = pPlayer->getHeight();

	for (int i = 0; i < objects.size(); i++)
	{
		if (objects[i]->type() == std::string("PowerUp") && objects[i]->isVisible() == true)
		{
			pRect2->x = objects[i]->getPosition().getX();
			pRect2->y = objects[i]->getPosition().getY();
			pRect2->w = objects[i]->getWidth();
			pRect2->h = objects[i]->getHeight();
			if (RectRect(pRect1, pRect2) && hitCount < 1)
			{
				hitCount++;
				delete pRect2;
				delete pRect1;
				pPlayer->checkPowerUp(objects);
				if (!objects[i]->dead() && !objects[i]->dying())
				{
					//pPlayer->usePower(objects);
					objects[i]->collision();
					
				}
			}
		}
	}
	
	
	
}

void CollisionManager::checkBossPlayerBulletCollision(const std::vector<GameObject*>& objects)
{
	for (int i = 0; i < objects.size(); i++)
	{
		GameObject* pObject = objects[i];

		for (int j = 0; j < TheBulletHandler::Instance()->getPlayerBullets().size(); j++)
		{
			if (pObject->type() != std::string("Boss") ||  !pObject->updating())
			{
				continue;
			}

			SDL_Rect* pRect1 = new SDL_Rect();
			pRect1->x = pObject->getPosition().getX();
			pRect1->y = pObject->getPosition().getY();
			pRect1->w = pObject->getWidth();
			pRect1->h = pObject->getHeight();

			PlayerBullet* pPlayerBullet = TheBulletHandler::Instance()->getPlayerBullets()[j];

			SDL_Rect* pRect2 = new SDL_Rect();
			pRect2->x = pPlayerBullet->getPosition().getX();
			pRect2->y = pPlayerBullet->getPosition().getY();
			pRect2->w = pPlayerBullet->getWidth();
			pRect2->h = pPlayerBullet->getHeight();



			if (RectRect(pRect1, pRect2))
			{
				if (!pObject->dying() && !pPlayerBullet->dying() && pObject->isVisible() == true)
				{
					pObject->collision();
					pPlayerBullet->collision();
				}
			}

			delete pRect1;
			delete pRect2;
		}
	}
}
void CollisionManager::checkPlayerBossCollision(Player* pPlayer, const std::vector<GameObject*> &objects)
{
	SDL_Rect* pRect1 = new SDL_Rect();
	pRect1->x = pPlayer->getPosition().getX();
	pRect1->y = pPlayer->getPosition().getY();
	pRect1->w = pPlayer->getWidth();
	pRect1->h = pPlayer->getHeight();

	for (int i = 0; i < objects.size(); i++)
	{
		if (objects[i]->type() != std::string("Boss") || !objects[i]->updating())
		{
			continue;
		}

		SDL_Rect* pRect2 = new SDL_Rect();
		pRect2->x = objects[i]->getPosition().getX();
		pRect2->y = objects[i]->getPosition().getY();
		pRect2->w = objects[i]->getWidth();
		pRect2->h = objects[i]->getHeight();

		if (RectRect(pRect1, pRect2))
		{
			if (!objects[i]->dead() && !objects[i]->dying())
			{
				if (objects[i]->isVisible() == true)
				{
					pPlayer->collision();
				}
			}
		}

		delete pRect2;
	}

	delete pRect1;
}





