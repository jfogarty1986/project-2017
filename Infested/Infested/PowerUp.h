//Power up is of type Enemy so it can be picked added to array easily and picked up

#ifndef POWERUP_H
#define POWERUP_H

#include "Enemy.h"
#include "Level.h"
#include "SoundManager.h"

class PowerUp : public Enemy 
{
public:
    
    virtual ~PowerUp() {}
	
    PowerUp() : Enemy()
    {
        m_dyingTime = 50;
        m_health = 1;
        m_moveSpeed = 1;
        m_bulletFiringSpeed = 0;
		visible = true;
		
    }
	virtual std::string type() { return "PowerUp"; }
    virtual void collision()
    {
        m_health -= 1;
        if(m_health == 0)
        {
            if(!m_bPlayedDeathSound)
            {
                TheSoundManager::Instance()->playSound("explode", 0);
                
                m_textureID = "largeexplosion";
                m_currentFrame = 0;
                m_numFrames = 9;
                m_width = 60;
                m_height = 60;
                m_bDying = true;
            }
            
        }
    }

    
    virtual void update()
    {
		if (TheGame::Instance()->getCurrentLevel() == 4)
		{
			m_moveSpeed = 0;
		}
		else
		{
			m_moveSpeed = 1;
		}
		//int target = 50;
		//int num = rand() % 2000;
		/*if (target == num)
		{
			visible = true;
		}*/

        if(!m_bDying)
        {   
			m_velocity.setX(-m_moveSpeed);
        }
        else
        {
            m_velocity.setY(0);
            doDyingAnimation();
        }

		if (visible == true)
		{
			m_textureID = "PowerUp";
		}
		if (visible == false)
		{
			m_textureID = "";
			m_velocity.setX(0);
		}
        
        ShooterObject::update();
    }

	

private:
	int move = 0;
	int counter = 0;
	Level *player;
};

class PowerUpCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new PowerUp();
    }
};


#endif
