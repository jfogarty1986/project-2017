
//Unused class
#ifndef SHOOTERENEMY_H
#define SHOOTERENEMY_H

#include "Enemy.h"
#include "Level.h"
#include "Player.h"

class ShooterEnemy : public Enemy 
{
public:
    
    virtual ~ShooterEnemy() {}
	
    ShooterEnemy() : Enemy()
    {
        m_dyingTime = 50;
        m_health = 11;
        m_moveSpeed = 0;
        m_bulletFiringSpeed = 0;
		
    }
	virtual std::string type() { return "Enemy"; }
    virtual void collision()
    {
		//m_textureID = "largeexplosion-old";
        m_health -= 1;
        if(m_health == 0)
        {
            if(!m_bPlayedDeathSound)
            {
				TheSoundManager::Instance()->playSound("explode", 0);

				//m_textureID = "largeexplosion-old";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;	
            }   
        }
    }

    
    virtual void update()
    {

        if(!m_bDying)
        {   
			m_angle = 90;
			if (rand() % 20 == 10)
			{
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 10, m_position.getY(), 12, 12, "bullet1", 1, Vector2D(0, -4));
				//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX() + 40, m_position.getY(), 12, 12, "bullet1", 1, Vector2D(0, -4));
			}
			
			
        }
        else
        {
            m_velocity.setY(0);
            doDyingAnimation();
        }

		
		if (m_health == 0)
		{
			m_textureID = "largeexplosion-old";
			m_velocity.setX(0);
			m_velocity.setY(0);
		}
		if (m_velocity.getX() < 0)
		{
			m_angle = 0;
		}
		else
		{
			m_angle = 180;
		}
        
        ShooterObject::update();
    }

	

private:
	int move = 0;
	int counter = 0;
	Level *player;
	bool right = false;
	bool left = false;
	
};

class ShooterEnemyCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new ShooterEnemy();
    }
};


#endif
