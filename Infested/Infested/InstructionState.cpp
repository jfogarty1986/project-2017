//
//  InstructionState.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 17/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include "InstructionState.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "TextureManager.h"
#include "AnimatedGraphic.h"
#include "Game.h"
#include "MenuButton.h"
#include "InputHandler.h"
#include "StateParser.h"

const std::string InstructionState::s_InstructionStateID = "INSTRUCTION";

void InstructionState::s_InstructionStateToMain()
{
	TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}


void InstructionState::update()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->update();
		}
	}
}

void InstructionState::render()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->draw();
		}
	}
}

bool InstructionState::onEnter()
{
	// parse the state
	StateParser stateParser;
	stateParser.parseState("assets/attack.xml", s_InstructionStateID, &m_gameObjects, &m_textureIDList);

	m_callbacks.push_back(0);
	m_callbacks.push_back(s_InstructionStateToMain);


	// set the callbacks for menu items
	setCallbacks(m_callbacks);

	m_loadingComplete = true;

	std::cout << "entering InstructionState\n";
	return true;
}

bool InstructionState::onExit()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->clean();
			delete m_gameObjects[i];
		}

		m_gameObjects.clear();
	}

	std::cout << m_gameObjects.size();

	// clear the texture manager
	for (int i = 0; i < m_textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
	}

	TheInputHandler::Instance()->reset();

	std::cout << "exiting InstructionState\n";
	return true;
}

void InstructionState::setCallbacks(const std::vector<Callback>& callbacks)
{
	// go through the game objects
	for (int i = 0; i < m_gameObjects.size(); i++)
	{
		// if they are of type MenuButton then assign a callback based on the id passed in from the file
		if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
		{
			MenuButton* pButton = dynamic_cast<MenuButton*>(m_gameObjects[i]);
			pButton->setCallback(callbacks[pButton->getCallbackID()]);
		}
	}
}

