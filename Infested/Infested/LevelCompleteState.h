//
//  LevelCompleteState.h
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 17/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#ifndef __SDL_Game_Programming_Book__LevelCompleteState__
#define __SDL_Game_Programming_Book__LevelCompleteState__

#include <iostream>
#include <vector>
#include "MenuState.h"

class GameObject;

class LevelCompleteState : public MenuState
{
public:

	virtual ~LevelCompleteState() {}

	virtual void update();
	virtual void render();

	virtual bool onEnter();
	virtual bool onExit();

	virtual std::string getStateID() const { return s_LevelCompleteID; }

	virtual void setCallbacks(const std::vector<Callback>& callbacks);

private:

	static void s_LevelCompleteToMain();
	static void s_ContinuePlay();

	static const std::string s_LevelCompleteID;

	std::vector<GameObject*> m_gameObjects;
};

#endif /* defined(__SDL_Game_Programming_Book__LevelCompleteState__) */
