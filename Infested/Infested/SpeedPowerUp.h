

#ifndef SpeedPowerUp_H
#define SpeedPowerUp_H

#include "PowerUp.h"
#include "Level.h"


class SpeedPowerUp : public PowerUp
{
public:
    
    virtual ~SpeedPowerUp() {}
	
    SpeedPowerUp() : PowerUp()
    {
        m_dyingTime = 50;
        m_health = 1;
        m_moveSpeed = 1;
        m_bulletFiringSpeed = 0;
		visible = true;
		
    }
	virtual std::string type() { return "PowerUp"; }
    virtual void collision()
    {
        m_health -= 1;
        if(m_health == 0)
        {
            if(!m_bPlayedDeathSound)
            {
                TheSoundManager::Instance()->playSound("explode", 0);
                
                m_textureID = "largeexplosion";
                m_currentFrame = 0;
                m_numFrames = 9;
                m_width = 60;
                m_height = 60;
                m_bDying = true;
            }
            
        }
    }

    
    virtual void update()
    {

        if(!m_bDying)
        {   
			m_velocity.setX(-m_moveSpeed);
        }
        else
        {
            m_velocity.setY(0);
            doDyingAnimation();
        }

		if (visible == true)
		{
			m_textureID = "SpeedPowerUp";
		}
		if (visible == false)
		{
			m_textureID = "";
		}
        
        ShooterObject::update();
    }

	

private:
	int move = 0;
	int counter = 0;
	Level *player;
};

class SpeedPowerUpCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new SpeedPowerUp();
    }
};


#endif
