
#ifndef ENEMY_H
#define ENEMY_H

#include <iostream>
#include "ShooterObject.h"
#include "BulletHandler.h"

// Enemy base class
class Enemy : public ShooterObject
{
public:
    
    virtual std::string type() { return "Enemy"; }
	bool spawnCrawlers() { activate = true; }
	bool spawnBoss() { CrawlersDead = true; }
	
protected:
    
    int m_health;
	bool activate = false;
	bool CrawlersDead = false;
    Enemy() : ShooterObject() {}
    virtual ~Enemy() {}
};

#endif /* defined(__SDL_Game_Programming_Book__Enemy__) */
