#ifndef AI_H
#define AI_H

//Unused Class

#include "ObjectLayer.h"
#include "Player.h"
#include "Enemy.h"
class AI
{
public:
	AI();

	void updateEnemyUpdate(Enemy*);

	void updatePlayerUpdate(Player*);


private:
protected:
};
#endif // !AI_H
