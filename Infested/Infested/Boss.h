#ifndef BOSS_H
#define BOSS_H

#include "Enemy.h"
#include "Level.h"
#include "SoundManager.h"
#

class Boss : public Enemy
{
public:

	virtual ~Boss() {}

	Boss() : Enemy()
	{
		m_dyingTime = 50;
		m_health = 25;
		m_moveSpeed = 1;
		m_bulletFiringSpeed = 0;
		visible = false;
	}

	virtual void collision()
	{
		m_health -= 1;
		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
				TheGame::Instance()->setLevelComplete(true);
			}

		}
	}
	virtual std::string type() { return "Boss"; }

	virtual void update()
	{

		if (!m_bDying)
		{

			if (SDL_GetTicks() % 48 == 1 && visible == true)
			{
				int num = rand() % 100;
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), num, num, "bullet1", 1, Vector2D(-3, 0));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), num, num, "bullet1", 1, Vector2D(3, 0));
				m_velocity.setX(m_moveSpeed);
				if (m_position.getX() < TheGame::Instance()->getGameWidth() / 2)
				{
					m_angle = 0;
				}
				else
				{
					m_angle = 180;
				}
			}

			if (move == 0)
			{
				m_velocity.setY(m_moveSpeed);
			}
			else if (move == 1)
			{
				m_velocity.setY(-m_moveSpeed);
				if (m_position.getY() <= 620 && m_position.getY() >= 610)
				{
					//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(-3, 0));
				}
			}
			else if (move == 3)
			{
				m_moveSpeed = 3;
				m_velocity.setX(-m_moveSpeed);
			}
			else if (move == 4)
			{
				m_velocity.setX(m_moveSpeed);
			}

			if (m_position.getY() <= 50)
			{
				move = 0;
			}

			else if (m_position.getY() >= TheGame::Instance()->getGameHeight() - 120)
			{
				move = 1;

			}
			else if (m_position.getX() <= 70)
			{
				move = 4;

			}
			else if (m_position.getX() >= TheGame::Instance()->getGameHeight() - 70)
			{
				move = 3;

			}
		}
		

		if (visible == true)
		{
			m_textureID = "boss";
		}
		if (visible == false)
		{
			m_textureID = "";
		}
		if (m_health == 0)
		{
			m_textureID = "explosion";
			m_velocity.setX(0);
			m_velocity.setY(0);
		}
		if (m_velocity.getX() < 0)
		{
			m_angle = 0;
		}
		else
		{
			m_angle = 180;
		}

		ShooterObject::update();

	}





private:
	int move = 0;
	int counter = 0;
	Level *player;
};

class BossCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new Boss();
	}
};


#endif

