

#ifndef __SDL_Game_Programming_Book__CollisionManager__
#define __SDL_Game_Programming_Book__CollisionManager__

#include <iostream>
#include <vector>

class Player;
class GameObject;
class TileLayer;

class CollisionManager
{
public:
    
    void checkPlayerEnemyBulletCollision(Player* pPlayer);
    void checkPlayerEnemyCollision(Player* pPlayer, const std::vector<GameObject*> &objects);
    void checkEnemyPlayerBulletCollision(const std::vector<GameObject*>& objects);
    void checkPlayerTileCollision(Player* pPlayer, const std::vector<TileLayer*>& collisionLayers);
	void checkEnemyBulletTileCollision(const std::vector<GameObject*> &objects, const std::vector<TileLayer*>& collisionLayers);
	void checkPowerUpCollision(Player* pPlayer, std::vector<GameObject*> &objects);
	void checkBossPlayerBulletCollision(const std::vector<GameObject*>& objects);
	void checkPlayerBossCollision(Player* pPlayer, const std::vector<GameObject*> &objects);
	//void banditMovement(const std::vector<GameObject*> &objects);
private:
	int playerX, playerY;
	int hitCount = 0;
};

#endif /* defined(__SDL_Game_Programming_Book__CollisionManager__) */
