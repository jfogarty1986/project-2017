
//Cars for level 2
#ifndef MERCCAR_H
#define MAERCAR_H

#include "Enemy.h"
#include "Level.h"
#

class MercCar : public Enemy 
{
public:
    
    virtual ~MercCar() {}
	
    MercCar() : Enemy()
    {
        m_dyingTime = 50;
        m_health = 100;
        m_moveSpeed = 10;
        m_bulletFiringSpeed = 0;
		
		
    }
	virtual std::string type() { return "Enemy"; }
    virtual void collision()
    {
		m_textureID = "explosion";
        //m_health -= 1;
        if(m_health == 0)
        {
            if(!m_bPlayedDeathSound)
            {
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
            }
            
        }
    }

    
	virtual void update()
	{

		if (!m_bDying)
		{
			m_velocity.setX(-m_moveSpeed);
			if (m_health == 0)
			{
				m_textureID = "explosion";
				m_velocity.setX(0);
				m_velocity.setY(0);
			}


			ShooterObject::update();
		}
	}
private:
	int move = 0;
	int counter = 0;
	Level *player;
};

class MercCarCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new MercCar();
    }
};


#endif
