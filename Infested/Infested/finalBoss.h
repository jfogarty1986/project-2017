
#ifndef FINALBOSS_H
#define FINALBOSS_H

#include "Enemy.h"
#include "Level.h"
#include "SoundManager.h"


class finalBoss : public Enemy
{
public:

	virtual ~finalBoss() {}

	finalBoss() : Enemy()
	{
		m_dyingTime = 50;
		m_health = 50;
		m_moveSpeed = 3;
		m_bulletFiringSpeed = 0;
		visible = true;
		
	}

	virtual void collision()
	{
		m_health -= 1;
		if (m_health == 0)
		{
			if (!m_bPlayedDeathSound)
			{
				TheSoundManager::Instance()->playSound("explode", 0);

				m_textureID = "largeexplosion";
				m_currentFrame = 0;
				m_numFrames = 9;
				m_width = 60;
				m_height = 60;
				m_bDying = true;
				TheGame::Instance()->setNextLevel(1);
				TheGame::Instance()->setLevelComplete(true);
				
			}

		}
	}
	virtual std::string type() { return "Enemy"; }

	virtual void update()
	{
		if (isVisible() == true)
		{
			
			m_textureID = "finalBoss";
		}
		else
		{
			
		}
	
		if (!m_bDying)
		{
			if (m_health <= 5)
			{
				m_textureID = "final2";
				m_width = 200;
				m_height = 200;
			}
			if (SDL_GetTicks() % 48 == 1 && visible == true)
			{
				//int num = rand() % 100;
				int bulletNumber = rand() % 10 + 1;
				bulletNumber = bulletArray[bulletNumber];
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 40, 40, 40, "bullet1", 1, Vector2D(-5, bulletNumber));
				bulletNumber = rand() % 10 + 1;
				bulletNumber = bulletArray[bulletNumber];
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY() + 140, 40, 40, "bullet1", 1, Vector2D(-5, bulletNumber));
				
			}
			int maxX = TheGame::Instance()->getGameWidth()- 40, maxY = TheGame::Instance()->getGameHeight() - 40, minX = 40, miny = 40;
			if (m_textureID == "finalBoss")
			{
				//m_velocity.setX(-6);
			}
			else if (m_textureID == "final2")
			{
				//m_velocity.setX(m_moveSpeed);
			}

			if (m_position.getY() < 50)
			{
				up = false;
			}
			else if (m_position.getY() > TheGame::Instance()->getGameHeight() - 100)
			{
				up = true;
			}

			if (up)
			{
				m_velocity.setY(-m_moveSpeed);
			}
			else if (!up)
			{
				m_velocity.setY(m_moveSpeed);
			}
			if (m_position.getX() > TheGame::Instance()->getGameWidth() - 100)
			{
				m_position.setX(TheGame::Instance()->getGameWidth() - 250);
			}
			
			
		}
		if (m_health == 0)
		{
			m_textureID = "explosion";
			m_velocity.setX(0);
			m_velocity.setY(0);
		}

		

		ShooterObject::update();

	}





private:
	int move = 0;
	int counter = 0;
	Level *player;
	bool up = false;
	int bulletArray[10] = { -5,-4,-3,-2,-1,0,1,2,3,4 };
};

class finalBossCreator : public BaseCreator
{
	GameObject* createGameObject() const
	{
		return new finalBoss();
	}
};


#endif


