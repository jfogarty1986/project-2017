//
//  LevelCompleteState.cpp
//  SDL Game Programming Book
//
//  Created by shaun mitchell on 17/02/2013.
//  Copyright (c) 2013 shaun mitchell. All rights reserved.
//

#include "LevelCompleteState.h"
#include "MainMenuState.h"
#include "PlayState.h"
#include "EndGameState.h"
#include "TextureManager.h"
#include "AnimatedGraphic.h"
#include "Game.h"
#include "MenuButton.h"
#include "InputHandler.h"
#include "StateParser.h"

const std::string LevelCompleteState::s_LevelCompleteID = "LEVELCOMPLETE";

void LevelCompleteState::s_LevelCompleteToMain()
{
	TheGame::Instance()->getStateMachine()->changeState(new MainMenuState());
}

void LevelCompleteState::s_ContinuePlay()
{
	if (TheGame::Instance()->getCurrentLevel() > 4)
	{
		TheGame::Instance()->setCurrentLevel(1);
		TheGame::Instance()->getStateMachine()->changeState(new EndGameState());
		
	}
	else
	{
		TheGame::Instance()->getStateMachine()->changeState(new PlayState());
	}
}

void LevelCompleteState::update()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->update();
		}
	}
}

void LevelCompleteState::render()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->draw();
		}
	}
}

bool LevelCompleteState::onEnter()
{
	// parse the state
	StateParser stateParser;
	stateParser.parseState("assets/attack.xml", s_LevelCompleteID, &m_gameObjects, &m_textureIDList);

	m_callbacks.push_back(0);
	m_callbacks.push_back(s_LevelCompleteToMain);
	m_callbacks.push_back(s_ContinuePlay);

	// set the callbacks for menu items
	setCallbacks(m_callbacks);

	m_loadingComplete = true;

	std::cout << "entering LevelCompleteState\n";
	return true;
}

bool LevelCompleteState::onExit()
{
	if (m_loadingComplete && !m_gameObjects.empty())
	{
		for (int i = 0; i < m_gameObjects.size(); i++)
		{
			m_gameObjects[i]->clean();
			delete m_gameObjects[i];
		}

		m_gameObjects.clear();
	}

	std::cout << m_gameObjects.size();

	// clear the texture manager
	for (int i = 0; i < m_textureIDList.size(); i++)
	{
		TheTextureManager::Instance()->clearFromTextureMap(m_textureIDList[i]);
	}

	TheInputHandler::Instance()->reset();

	std::cout << "exiting LevelCompleteState\n";
	return true;
}

void LevelCompleteState::setCallbacks(const std::vector<Callback>& callbacks)
{
	// go through the game objects
	for (int i = 0; i < m_gameObjects.size(); i++)
	{
		// if they are of type MenuButton then assign a callback based on the id passed in from the file
		if (dynamic_cast<MenuButton*>(m_gameObjects[i]))
		{
			MenuButton* pButton = dynamic_cast<MenuButton*>(m_gameObjects[i]);
			pButton->setCallback(callbacks[pButton->getCallbackID()]);
		}
	}
}

