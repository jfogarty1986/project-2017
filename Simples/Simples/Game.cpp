#include "Game.h"
#include "TextureManager.h"
#include "GameObject.h"
#include "Map.h"

GameObject* player;
GameObject *enemy;
Map* map;
//will be reassigned upon creation of renderer
SDL_Renderer* Game::renderer = nullptr;

Game::Game()
{

}

Game::~Game()
{

}

void Game::init(char* title, int xPos, int yPos, int width, int height, bool fullscreen)
{
	int flags = 0;
	if (fullscreen == true)
	{
		flags = SDL_WINDOW_FULLSCREEN;
	}
	if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
	{
		std::cout << "Subsystems initalised\n";

		window = SDL_CreateWindow(title, xPos, yPos, width, height, flags);
		if (window)
		{
		std::cout << "Window Created\n";
		}
		renderer = SDL_CreateRenderer(window, -1, 0);
		if (renderer)
		{
			SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
			std::cout << "Renderer Created\n";
		}
		isRunning = true;
	}
	else
	{
		isRunning = false;
	}

	player = new GameObject("assets/player.png",0,0);
	enemy = new GameObject("assets/zombie-1.png", 100, 100);
	map = new Map();
}

void Game::handleEvents()
{
	SDL_Event event;
	SDL_PollEvent(&event);
	switch (event.type)
	{
	case SDL_QUIT:
		isRunning = false;
		break;
	default:
		break;
	}
}

void Game::update()
{
	count++;
	player->update();
	enemy->update();

	std::cout << count << std::endl;
}

void Game::render()
{
	SDL_RenderClear(renderer);
	map->drawMap();
	player->render();
	enemy->render();
	SDL_RenderPresent(renderer);
}

void Game::clean()
{
	SDL_DestroyWindow(window);
	SDL_DestroyRenderer(renderer);
	SDL_Quit;
	std::cout << "Game is cleaned\n";
}