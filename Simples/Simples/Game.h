#pragma once

#include <SDL.h>
#include "SDL_image.h"
#include <iostream>

class Game
{
public:
	Game();
	~Game();

	void init(char* title, int xPos, int yPos, int width, int height, bool fullscreen);
	void handleEvents();

	void update();
	void render();
	void clean();
	
	bool running() {return isRunning;}

	static SDL_Renderer *renderer;

private:
	int count;
	bool isRunning;
	SDL_Window *window;
	//SDL_Renderer *renderer;
};


