#pragma once
#include "GameObject.h"

class ShooterObject
{
public:
	ShooterObject();
	~ShooterObject();
	void setBulletSpeed(int);
protected:
	int bulletSpeed = 5;
	int bulletCounter = 0;

};
