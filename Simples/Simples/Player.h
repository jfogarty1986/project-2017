#pragma once
#include "GameObject.h"
#include "ShooterObject.h"

class Player : public ShooterObject
{
public:
	Player(int,int);
	~Player();
	bool isAlive() { return alive; }
protected:
	int xPos;
	int yPos;
	int health;
	int lives = 3;
	bool alive = true;
};


