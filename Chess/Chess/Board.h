#pragma once
#include <iostream>
#define ROW 8
#define COL 8

class Board
{
public:
	Board();
	~Board();
	void printBoard();
private:
	int board[ROW][COL];
};
