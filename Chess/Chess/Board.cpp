#include "Board.h"

using namespace std;

Board::Board()
{
	for (int i = 0; i < ROW; i++)
	{
		for (int j = 0; j < COL; j++)
		{
			board[i][j] = 0;
		}
	}
}

void Board::printBoard()
{
	for (int i = 0; i < ROW; i++)
	{
		cout << "\t";
		for (int j = 0; j < COL; j++)
		{
			cout << board[i][j] << "\t";
		}
		cout << endl;
		cout << endl;
		cout << endl;
		cout << endl;
	}
}